#!/bin/bash

# what to backup
backup_files="/home/myuser/myfolder"

# where to backup to
dest="/home/myuser/backup"

# create archive filename
day=$(date %A)
hostname=$(hostname -s)
archive_file="$hostname-$day.tgz"

# print start status message
echo "Backng up $bakups_file to $dest/$archive_file"
date
echo

# backup files using tar
tar czf $dest/$archive_file $backup_files

# print end status message
echo
echo "Backup finished"
