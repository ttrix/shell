#!/bin/sh

echo "simple config or server"

file_name=config.yaml

if [ -d "config" ]
then
  echo "reading config directory"
  config_files=$(ls config)
else
  echo "config directory not found. Creating config dir"
  mkdir config
fi

echo "using file $file_name to ..."
echo "here are all configuration files: $config_files"

user_group=$1
