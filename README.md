# Shell scripting

## Types of shell scripts

- #!/bin/sh   => SH
- #!/bin/bash => BASH
- #!/bin/zsh  => ZSH

## Useful commands but not used often

df                              know the size of the system files
du -hs folder_name              get the size of the disk used by a particular folder


## variables

- Declare the variable
file_name=config.yaml
There must be NO SPACES around the '=' sign

- Use $ to use the variable
echo "using file $file_name ..."

- Store output of an executed command in a variable
config_files=$(ls config)

## conditionals

- basics
if [ .. ]
then
  ...
elif [ ... ]
then
  ...
else
  ...
fi

### File test operators
Check if file_name 
- is a directory. It can be used to know if a dir exists
if [ -d file_name ] ..
- is a file
if [ -f file_name ] ..
- is readable
if [ -r file_name ]
- is writable 
if [ -w file_name ]
- is executable
if [ -x file_name ]

### Relational operators
if [ num_files -eq 10]
==  -eq
!=  -ne
>=  -ge
<=  -le
>   -gt
<   -lt

### String operators
var_contains_some_string=abc
if [ "$var_contains_some_string" == "qsd" ]   # do not forget to use $ to reference the variable + put the variable inside ""
==  equals (or POSIX syntax = )
!=
-z  length of the string is 0
-n  length is not zero

## Passing parameters 
Command line:
./script.sh arg1 arg2 arg3 ..

Using the parameter/argumet inside the script
$1  arg1
$2  arg2
$3  arg3
...

$0  special arg which contains the name of the script/file being executed

ie:
...
some_var=$1
...

### extra options

$*  it stores all the parameters past to the script
$#  return how many parameters has been past to the script 

## Passing parameters from prompt (asking the user to type values)

read -p "Please enter your value: " var_storing_value

### use value past in prompt

"$var_storing_value"

## Loops

for param in $*       # $* contains all the parameters past to the script
  do
    echo $param
  done

while [ ]
  do
    ...
  done

## Arithmetic operations VS Concatenation

* Given values
val1=2
val2=6

### Concatenation
value=$val1+$val2   # value is 26

### Sum
value=$(( $val1 + val2 ))   # value is 8


## Functions

function function_name {
  ...
}

### Invoke the function

Simply by the function name without any sing like quotes or $

### Pass parameters to functions

function func_name() {
  ...
  var1=$1   # $1 first parameter
  ...
}

func_name param1

-> Best practice: do not use more than 5 parameters in a function

### returning values from functions

-use return key word

function sum() {
  total=$(($1+$2))
  return $total
}

#### 2 option to get the returned value

- execute command and store result
result=$(sum 7 9)

- use $? (dollar sign + question mark) to access the result of the previous command execution
sum 7 9
result=$?


## Environment variables

printenv    print all the env vars configured in the system for the specific user

### print a specific env var

printenv USER     it will only print the value for USER env var

### Create environment variables

#### for the current session

export MY_ENV_VAR=var_value   # create variable

unset MY_ENV_VAR              # remove variable

#### add variable permanently for the user

Add the variable in the Shell specific configuration file ~/.bashrc (or .shrc/.zshrc when using sh/zsh as shell program)
Variables used in this file are loaded whenever a bash login shell is entered.

Adding in the same way as above:

export MY_ENV_VAR=var_value

To load the latest changes we need to close/open the terminal OR run this command:

source ~/.bashrc

## Create our own command

- Create a script with the suitable code (add shebang at the beginning #!/bin/bash)
- Make the script executable for all users (chmode a+x myscript)
- Add the folder location (ie. /home/myuser) in the right config file according with who should have access to the command (specific user or all users)

### Accessible for a specific user

Add the location of the folder containing the script to the PATH variable  in the ~/.bashrc file. 

ie: if the executable script is located in myuser's home directory
PATH=$PATH:/home/myuser

### Accessible for all users

Add the location of the folder containing the script as part (ie. at the end) of the PATH variable stored here:

/etc/environment

