#!/bin/bash

-x    proxy
-X HHTP method

# using proxy
curl -vvk -x http://172.12.12.12:8080 https://abc.url.com

# requesting POST api
curl -vvk -X POST -x http://172.12.12.12:8080 -H "Content-Type=x-www-form-urlencoded" -d "param1=value1&param2=value2" https://abc.url.com